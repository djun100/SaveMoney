package com.zhaoj.savemoney.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class AccountDBHelper extends SQLiteOpenHelper {
	private static final String CREATE_COSTTYPETBL_SQL_STRING = "create table costtype_tbl"
			+ "(costtype_id integer primary key autoincrement,"
			+ "costtype_name varchar(64),"
			+ "costtype_icon_id integer,"
			+ "costtype_kind integer)";
	private static final String CREATE_ACOUNTTBL_SQL_STRING = "create table default_account_tbl"
			+ "(defacu_id integer primary key autoincrement, "
			+ "defacu_time date,"
			+ "defacu_amount double,"
			+ "defacu_costtype integer references costtype_tbl(costtype_id),"
			+ "defacu_remark text," + "defacu_kind integer)";

	private static final String DATABASE_NAME = "acount.db3";
	public static final int DATABASE_VERSION = 2;

	private Context mContext;

	public AccountDBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		mContext = context;
	}

	public AccountDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		mContext = context;
	}

	@SuppressLint("NewApi")
	public AccountDBHelper(Context context, String name, CursorFactory factory,
			int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
		mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createDBSchma(db);
		InitCostTypeTable(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	private void createDBSchma(SQLiteDatabase db) {
		db.execSQL(CREATE_COSTTYPETBL_SQL_STRING);
		db.execSQL(CREATE_ACOUNTTBL_SQL_STRING);
	}

	private void InitCostTypeTable(SQLiteDatabase db) {
		String[] costTypes;
		String[] CostTypeICONIDs;
		ContentValues values = new ContentValues();

		costTypes = mContext.getResources().getStringArray(
				com.zhaoj.savemoney.R.array.costtype_out);
		CostTypeICONIDs = mContext.getResources().getStringArray(
				com.zhaoj.savemoney.R.array.costtype_icon_out);
		for (int i = 0; i < costTypes.length; i++) {
			String costtype = costTypes[i];
			values.clear();
			values.put("costtype_name", costtype);
			values.put("costtype_kind", 0);
			values.put(
					"costtype_icon_id",
					mContext.getResources().getIdentifier(CostTypeICONIDs[i],
							"drawable", mContext.getPackageName()));
			db.insert("costtype_tbl", null, values);
		}

		costTypes = mContext.getResources().getStringArray(
				com.zhaoj.savemoney.R.array.costtype_in);
		CostTypeICONIDs = mContext.getResources().getStringArray(
				com.zhaoj.savemoney.R.array.costtype_icon_in);
		for (int i = 0; i < costTypes.length; i++) {
			String costtype = costTypes[i];
			values.clear();
			values.put("costtype_name", costtype);
			values.put("costtype_kind", 1);
			values.put(
					"costtype_icon_id",
					mContext.getResources().getIdentifier(CostTypeICONIDs[i],
							"drawable", mContext.getPackageName()));
			db.insert("costtype_tbl", null, values);
		}
	}
}
