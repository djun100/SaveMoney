package com.zhaoj.savemoney.ui;

import cn.kuaipan.android.openapi.AuthActivity;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.service.DownloadService;
import com.zhaoj.savemoney.utils.ConstantsUtils;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity implements
		OnCheckedChangeListener {
	private static final String TAG = "MainActivity";
	public static long exitTime = 0;
	private TabHost mTabHost;
	private Intent mDetaIntent;
	private Intent mChartIntent;
	private Intent mBookIntent;
	private Intent mSettingIntent;
	private Intent mSendMail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		ApplicationData.MainActivitycontext = this;
		ApplicationData.mainActivity = this;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.view_maintabs);

		this.mDetaIntent = new Intent(this, DetailActivity.class);
		this.mChartIntent = new Intent(this, ChartActivity.class);
		this.mSettingIntent = new Intent(this, SettingsActivity.class);
		this.mBookIntent = new Intent(this, AcountBookShelfActivity.class);
		this.mSendMail = new Intent(this, MailActivity.class);

		((RadioButton) findViewById(R.id.radio_button0))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.radio_button1))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.radio_button2))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.radio_button3))
				.setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.radio_button4))
				.setOnCheckedChangeListener(this);
		setupIntent();
	}

	private void setupIntent() {
		this.mTabHost = getTabHost();
		TabHost localTabHost = this.mTabHost;

		localTabHost.addTab(buildTabSpec("Detail_TAB", R.string.main_detail,
				R.drawable.icon_shopping, this.mDetaIntent));
		localTabHost.addTab(buildTabSpec("Chart_TAB", R.string.main_chart,
				R.drawable.icon_chart, this.mChartIntent));
		localTabHost.addTab(buildTabSpec("Book_TAB", R.string.main_book,
				R.drawable.icon_book, this.mBookIntent));
		localTabHost.addTab(buildTabSpec("Mail_TAB", R.string.main_mail,
				R.drawable.icon_mail, this.mSendMail));
		localTabHost.addTab(buildTabSpec("Setting_TAB", R.string.main_setting,
				R.drawable.icon_set, this.mSettingIntent));
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	private TabHost.TabSpec buildTabSpec(String tag, int resLabel, int resIcon,
			final Intent content) {
		return this.mTabHost
				.newTabSpec(tag)
				.setIndicator(getString(resLabel),
						getResources().getDrawable(resIcon))
				.setContent(content);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			switch (buttonView.getId()) {
			case R.id.radio_button0:
				this.mTabHost.setCurrentTabByTag("Detail_TAB");
				break;
			case R.id.radio_button1:
				this.mTabHost.setCurrentTabByTag("Chart_TAB");
				break;
			case R.id.radio_button2:
				this.mTabHost.setCurrentTabByTag("Book_TAB");
				break;
			case R.id.radio_button3:
				this.mTabHost.setCurrentTabByTag("Mail_TAB");
				break;
			case R.id.radio_button4:
				this.mTabHost.setCurrentTabByTag("Setting_TAB");
				break;
			}
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exit();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void exit() {
		if ((System.currentTimeMillis() - exitTime) > 20000) {
			Toast.makeText(
					ApplicationData.MainActivitycontext.getApplicationContext(),
					"再按一次退出程序", Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			finish();
			System.exit(0);
		}
	}

	private String getAuthResultName(int resultCode) {
		if (ApplicationData.authType == AuthActivity.CANCELED) {
			return "[CANCELED]";
		} else if (ApplicationData.authType == AuthActivity.FAILED) {
			return "[FAILED]";
		} else if (ApplicationData.authType == AuthActivity.SUCCESSED) {
			return "[SUCCESSED]";
		}
		return "[UnKnown]";
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.v(TAG, "requestCode:" + requestCode + " resultCode:" + resultCode);

		if (requestCode == AuthActivity.AUTH_REQUEST_CODE) {

			Log.v(TAG, "resultCode: " + getAuthResultName(resultCode));
			Log.v(TAG, "ResultData:  " + data);

			if (data == null) {
				return;
			}

			if (resultCode == AuthActivity.SUCCESSED && data != null) {
				Bundle values = data.getExtras();
				final String accessToken = values
						.getString(AuthActivity.EXTRA_ACCESS_TOKEN);
				final String accessSecret = values
						.getString(AuthActivity.EXTRA_ACCESS_SECRET);
				final String uid = values.getString(AuthActivity.EXTRA_UID);
				ApplicationData.authType = values
						.getInt(AuthActivity.EXTRA_AUTH_TYPE);

				Log.v(TAG, "Authorized by "
						+ getAuthName(ApplicationData.authType) + " server");

				final String error_code = values
						.getString(AuthActivity.EXTRA_ERROR_CODE);
				Log.v(TAG, "!!!accessToken=" + accessToken + "\n"
						+ "accessSecret=" + accessSecret + "\n" + "uid=" + uid
						+ "\n" + "error_code" + error_code);

				/**
				 * set validate access token pair, then isAuthorized will return
				 * true
				 */
				ApplicationData.mApi.setAccessToken(accessToken, accessSecret);
				// store all concern values into preference
				storeKeys(accessToken, accessSecret, uid,
						String.valueOf(ApplicationData.authType));

				Intent intent = new Intent(this, DownloadService.class);
				startService(intent);
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private void storeKeys(String key, String secret, String uid,
			String authType) {
		// Save the access key for later
		SharedPreferences prefs = getSharedPreferences(
				ConstantsUtils.ACCOUNT_PREFS_NAME, 0);
		Editor edit = prefs.edit();
		edit.putString(ConstantsUtils.ACCESS_KEY_NAME, key);
		edit.putString(ConstantsUtils.ACCESS_SECRET_NAME, secret);
		edit.putString(ConstantsUtils.ACCESS_UID_NAME, uid);
		edit.putString(ConstantsUtils.ACCESS_AUTH_TYPE_NAME, authType);
		edit.commit();
	}

	private String getAuthName(int authType) {
		if (authType == AuthActivity.WEIBO_AUTH) {
			return "[weibo]";
		} else if (authType == AuthActivity.QQ_AUTH) {
			return "[qq]";
		} else if (authType == AuthActivity.XIAOMI_AUTH) {
			return "[xiaomi]";
		} else if (authType == AuthActivity.KUAIPAN_AUTH) {
			return "[kuaipan]";
		}
		return "[UnKnown]";
	}
}
