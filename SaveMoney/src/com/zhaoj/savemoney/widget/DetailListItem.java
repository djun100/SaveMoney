package com.zhaoj.savemoney.widget;

import java.util.ArrayList;

public class DetailListItem {

	private int id;
	private String name;
	private int drawableID;
	private double amount;
	private String remark;
	private ArrayList<DetailListItem> groups;

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDrawableID() {
		return drawableID;
	}

	public void setDrawableID(int drawableID) {
		this.drawableID = drawableID;
	}

	public ArrayList<DetailListItem> getGroups() {
		return groups;
	}

	public void setGroups(ArrayList<DetailListItem> groups) {
		this.groups = groups;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
